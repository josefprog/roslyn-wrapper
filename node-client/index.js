'use strict'

let fs = require('fs')
let RestCall = require('./lib/rest-call')

let fileName = './node-client/code.cs'

function send () {
  return new Promise(function (resolve, reject) {
    // read cs file
    let file = fs.readFileSync(fileName).toString()
    console.log('Source file read...')

    let tokenCall = new RestCall()

    let options = {
      host: 'localhost',
      port: 51576,
      path: '/tokenize?format=json',
      method: 'POST'
    }

    console.log('Sending REST call...')
    tokenCall.execute(options, file)
      .then(function (results) {
        console.log('Got results: \r\n' + JSON.stringify(results, null, 2))
        // Do other stuff here?
        resolve(results)
      })
      .catch(function (err) {
        console.log('Error in web call: ' + err)
        reject(err)
      })
  })
}

send()
  .then(function (results) {
    console.log('Done!')
  })
