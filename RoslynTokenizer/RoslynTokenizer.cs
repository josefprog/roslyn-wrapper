﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Http;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Classification;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.Text;
using ServiceStack.Text;

namespace RoslynTokenizer
{
    public class Tokenize
    {
        public static string TokenizeCode(string text)
        {
            var _ = typeof(Microsoft.CodeAnalysis.CSharp.Formatting.CSharpFormattingOptions);
            var tree = SyntaxFactory.ParseSyntaxTree(text);
            var root = tree.GetRoot();

            AdhocWorkspace workspace = new AdhocWorkspace();

            var mscorlib = MetadataReference.CreateFromFile(typeof(object).Assembly.Location);
            var compilation = CSharpCompilation.Create("MyCompilation",
                syntaxTrees: new[] { tree }, references: new[] { mscorlib });

            var model = compilation.GetSemanticModel(tree);
            var classifiedSpans = Classifier.GetClassifiedSpans(model, TextSpan.FromBounds(0, text.Length), workspace);
            var source = tree.GetText();

            //var ranges = classifiedSpans.Select(classifiedSpan =>
            //    new Range(classifiedSpan, source.GetSubText(classifiedSpan.TextSpan).ToString()));

            var ranges = classifiedSpans.Select(classifiedSpan =>
                new Range(classifiedSpan, source.GetSubText(classifiedSpan.TextSpan).ToString()));

            ranges = FillGaps(source, ranges);
            var results = new List<Range>();

            foreach (var range in ranges)
            {
                var node = root.FindNode(range.TextSpan);
                range.Kind = node.Kind().ToString();
                results.Add(range);
            }

            var json = JsonSerializer.SerializeToString(results);

            return json;
        }

        private static IEnumerable<Range> FillGaps(SourceText text, IEnumerable<Range> ranges)
        {
            const string whitespaceClassification = null;
            int current = 0;
            Range previous = null;

            foreach (Range range in ranges)
            {
                int start = range.TextSpan.Start;
                if (start > current)
                {
                    yield return new Range(whitespaceClassification, TextSpan.FromBounds(current, start), text);
                }

                if (previous == null || range.TextSpan != previous.TextSpan)
                {
                    yield return range;
                }

                previous = range;
                current = range.TextSpan.End;
            }

            if (current < text.Length)
            {
                yield return new Range(whitespaceClassification, TextSpan.FromBounds(current, text.Length), text);
            }
        }
    }

    public class Range
    {
        public string Text { get; }

        public Range(string classification, TextSpan span, SourceText text) :
            this(classification, span, text.GetSubText(span).ToString())
        {
        }

        public Range(string classification, TextSpan span, string text) :
            this(new ClassifiedSpan(classification, span), text)
        {
        }

        public Range(ClassifiedSpan classifiedSpan, string text)
        {
            TextSpan = classifiedSpan.TextSpan;
            Text = text;
            ClassificationType = classifiedSpan.ClassificationType ?? "whitespace";
        }

        public string ClassificationType { get; }

        public TextSpan TextSpan { get; }
        public string Kind { get; set; }
    }
}

