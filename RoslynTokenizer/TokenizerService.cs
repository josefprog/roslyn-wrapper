﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ServiceStack;

namespace RoslynTokenizer
{
    public class TokenizeService : Service
    {
        public object Post(TokenizeRequest request)
        {
            var rawBody = base.Request.GetRawBody().Replace("\\r\\n", "\r\n");
            var body = rawBody.Substring(0, rawBody.Length - 1).Substring(1);
            var result = Tokenize.TokenizeCode(body);

            return result;
        }
    }

    public class TokenizeRequest
    {

    }
}
