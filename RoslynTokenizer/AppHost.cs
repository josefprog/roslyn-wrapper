﻿using System.Net;
using System.Reflection;
using Funq;
using ServiceStack;
using ServiceStack.Text;

namespace RoslynTokenizer
{
    public class AppHost : AppHostBase
    {
        public AppHost() : base("Tokenize", typeof(TokenizeService).GetTypeInfo().Assembly) { }

        public override void Configure(Container container)
        {
            this.CustomErrorHttpHandlers.Remove(HttpStatusCode.Forbidden);
            JsConfig.EmitCamelCaseNames = true;

            Routes.Add<TokenizeRequest>("/tokenize", ApplyTo.Post);
            Routes.Add<TolkienizeRequest>("/tolkienize", ApplyTo.Get);
        }
    }
}
