"""
Tokenize Module for testing Roslyn document parsing from python
"""

# Import libraries
import json
import requests

# Read in C# file
file = open("python-client\\code.cs", "r")
code = file.read()

# Setup http call
url = "http://localhost:51576/tokenize?format=json"
data = code
data_json = json.dumps(data)
#headers = {'Content-type': 'application/json'}

# Call http endpoint and get response
#response = requests.post(url, data=data_json, headers=headers)
response = requests.post(url, data=data_json)

# Show results
parsed = json.loads(response.text)
print(json.dumps(parsed, indent=2, sort_keys=True))
