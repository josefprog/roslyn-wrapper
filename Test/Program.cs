﻿using System;
using System.IO;
using RoslynWrapper;

namespace Test
{
    class Program
    {
        static void Main()
        {
            var filePath = "C:\\dev\\test.cs";
            var text = File.ReadAllText(filePath);

            var length = Consume.GetCodeLength(text);

            Console.WriteLine("Output Length = " + length);

            Console.ReadLine();
        }
    }
}
