
// How to read the Body from a request in to a variable
private string GetDocumentContents(System.Web.HttpRequestBase Request)
{
    string documentContents;
    using (Stream receiveStream = Request.InputStream)
    {
        using (StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8))
        {
            documentContents = readStream.ReadToEnd();
        }
    }
    return documentContents;
}


// Also add this to AppHost: Configure method is ServiceStack Module
// ServiceStack.Text.JsConfig.EmitCamelCaseNames = true;