# RoslynTokenizer
The Roslyn Tokenizer is a service that utilizes the Roslyn Code Analysis engine to create highly parsable tokens from .Net C# code and injects it in to the common database of code tokens.

## Inputs
 - Service takes in a C# file (contents) as body text.
 - Should this be an attached file (multipart form) insead?
 - Need to include other form details like project name, repo name, change date, repo url


## Outputs
Output should be forked.  Output to DB should be full tokenset with metadata.  Other output should include reponse with status code and result (x tokens added to db for project y in repo z)

## Data Store
Utilize Redis Search

Data in the form of:

```json
{
  "repo": "RoslynWrapper",
  "repoUrl": "https://github.com/echefjosef/RoslynWrapper.git",
  "projectName": "RoslynTokenizer",
  "updateDate": "12/5/2017 13:23:06",
  "lastCommitByUser": "echefjosef",
  "tokenSet": [
    {
      "codePhrase": "using System.Collections.Generic;",
      "tokenType": "UsingDirective",
      "classification": "Identifier",
      "identifier": "System.Collections.Generic",
      "span": "[14..45]",
      "tokens": [
        {
          "text": "using",
          "classificationType": "identifier",
          "textSpan": "[277..283)",
          "kind": "UsingDirective"
        },
        {
          "text": " ",
          "classificationType": "whitespace",
          "textSpan": "[283..284)",
          "kind": "IdentifierName"
        },
        {
          "text": "ServiceStack",
          "classificationType": "identifier",
          "textSpan": "[284..296)",
          "kind": "IdentifierName"
        },
        {
          "text": ".",
          "classificationType": "operator",
          "textSpan": "[296..297)",
          "kind": "ExplicitInterfaceSpecifier"
        },
        {
          "text": "Text",
          "classificationType": "identifier",
          "textSpan": "[297..301)",
          "kind": "MethodDeclaration"
        },
        {
          "text": ";",
          "classificationType": "punctuation",
          "textSpan": "[301..302)",
          "kind": "MethodDeclaration"
        }
      ]
    },
    {
      "codePhrase": "namepsace RoslynWrapper",
      "tokenType": "",
      "classification": "NamespaceDeclaration",
      "identifier": "",
      "span": "[14..45]",
      "tokens": [
        {
          "text": "nnamespace",
          "classificationType": "identifier",
          "textSpan": "[305..315)",
          "kind": "IdentifierName"
        },
        {
          "text": " ",
          "classificationType": "whitespace",
          "textSpan": "[315..316)",
          "kind": "IdentifierName"
        },
        {
          "text": "RoslynTokenizer",
          "classificationType": "identifier",
          "textSpan": "[316..331)",
          "kind": "VariableDeclarator"
        }
      ]
    },
    {
      "codePhrase": "public class Tokenizer",
      "tokenType": "",
      "classification": "ClassDeclaration",
      "identifier": "",
      "span": "[48..135]",
      "tokens": [
        {
          "text": "public",
          "classificationType": "keyword",
          "textSpan": "[340..346)",
          "kind": "ClassDeclaration"
        },
        {
          "text": " ",
          "classificationType": "whitespace",
          "textSpan": "[346..347)",
          "kind": "ClassDeclaration"
        },
        {
          "text": "class",
          "classificationType": "keyword",
          "textSpan": "[347..352)",
          "kind": "ClassDeclaration"
        },
        {
          "text": " ",
          "classificationType": "whitespace",
          "textSpan": "[352..353)",
          "kind": "ClassDeclaration"
        },
        {
          "text": "Tokenize",
          "classificationType": "class name",
          "textSpan": "[353..361)",
          "kind": "ClassDeclaration"
        }
      ]
    },
    {
      "codePhrase": "public static string TokenizeCode(string code)",
      "tokenType": "",
      "classification": "MethodDeclaration",
      "identifier": "",
      "span": "[142..232]",
      "tokens": [
        {
          "text": "public",
          "classificationType": "keyword",
          "textSpan": "[378..384)",
          "kind": "MethodDeclaration"
        },
        {
          "text": " ",
          "classificationType": "whitespace",
          "textSpan": "[384..385)",
          "kind": "MethodDeclaration"
        },
        {
          "text": "static",
          "classificationType": "keyword",
          "textSpan": "[385..391)",
          "kind": "MethodDeclaration"
        },
        {
          "text": " ",
          "classificationType": "whitespace",
          "textSpan": "[391..392)",
          "kind": "MethodDeclaration"
        },
        {
          "text": "string",
          "classificationType": "keyword",
          "textSpan": "[392..398)",
          "kind": "PredefinedType"
        },
        {
          "text": " ",
          "classificationType": "whitespace",
          "textSpan": "[398..399)",
          "kind": "PredefinedType"
        },
        {
          "text": "TokenizeCode",
          "classificationType": "identifier",
          "textSpan": "[399..411)",
          "kind": "MethodDeclaration"
        },
        {
          "text": "(",
          "classificationType": "punctuation",
          "textSpan": "[411..412)",
          "kind": "ParameterList"
        },
        {
          "text": "string",
          "classificationType": "keyword",
          "textSpan": "[412..418)",
          "kind": "PredefinedType"
        },
        {
          "text": " ",
          "classificationType": "whitespace",
          "textSpan": "[418..419)",
          "kind": "PredefinedType"
        },
        {
          "text": "code",
          "classificationType": "identifier",
          "textSpan": "[419..423)",
          "kind": "Parameter"
        },
        {
          "text": ")",
          "classificationType": "punctuation",
          "textSpan": "[423..424)",
          "kind": "ParameterList"
        }
      ]
    }
  ]
}

```
>>>>>>> master
